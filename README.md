Stripped to minimal code base for running hapConstructor.  This is a derivative (subset)
of original multifaceted haplotyping code from Thomas and Aho: It is only capable of 
a chi-square test.
