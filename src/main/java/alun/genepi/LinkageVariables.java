package alun.genepi;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import alun.genio.BasicGeneticData;
import alun.markov.GraphicalModel;
import alun.markov.Product;
import alun.markov.Variable;

public class LinkageVariables extends LociVariables
{
	public LinkageVariables(BasicGeneticData data, double errorpri)
	{
		super(data,errorpri);
	}

	public LinkageVariables(BasicGeneticData data)
	{
		this(data, -1);
	}

	public Product genericPriorProduct(int first, Genotype[] genos)
	{
		Product p = new Product();

		for (int i=first; i<loc.length; i++)
			p.add(new GenotypePrior(genos[i],d.alleleFreqs(i)));

		return p;
	}

	public void makeInheritances(int first)
	{
		for (int i=first; i<loc.length; i++)
			if (loc[i].patin == null)
				loc[i].makeInheritances();
	}

	public Product founderPriorProduct(int first)
	{
		Product p = new  Product();
		LocusVariables[] l = getLocusVariables();
		for (int i=first; i<l.length; i++)
			l[i].addFounderPriors(p);
		return p;
	}

	

/*
	// This version is doesn't depend on thep prepeeling of the locus products
	// which may be useful for debugging.
	public Product peeledSetOfMeiosesProduct(int[] x, int first)
	{	
		Product p = new Product();
		for (int j=first; j<loc.length; j++)
		{
			p.add(locusProduct(j).getFunctions());
			if (j > first)
			{
				for (int i=0; i<x.length; i++)
				{
					p.add(new FixedRecombination(getInheritance(x[i],j,0),getInheritance(x[i],j-1,0),d.getMaleRecomFrac(j,j-1)));
					p.add(new FixedRecombination(getInheritance(x[i],j,1),getInheritance(x[i],j-1,1),d.getFemaleRecomFrac(j,j-1)));
				}
			}
		}
		return p;
	}
*/

       public void report()
	{
		for (int i=1; i<loc.length; i++)
		{
			for (int j=0; j<loc[i].genotypes().length; j++)
				if (getInheritance(j,i,0) != null)
					System.out.print(getInheritance(j,i,0).getState()+""+getInheritance(j,i,1).getState()+" ");
			System.out.println();
		}
		System.out.println();
	}

	public void save(int first)
	{
		for (int i=first; i<loc.length; i++)
			loc[i].save();
	}
		
	public void restore(int first)
	{
		for (int i=first; i<loc.length; i++)
			loc[i].restore();
	}
}
