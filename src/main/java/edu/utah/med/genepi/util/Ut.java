//******************************************************************************
// Ut.java
//******************************************************************************
package edu.utah.med.genepi.util;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

//==============================================================================
public final class Ut {

	public static final String N = System.getProperty("line.separator");
	public static final String FS = File.separator;

	//----------------------------------------------------------------------------
	public static String pkgOf(Class c)
	{
		return longestPrefixOf(c.getName(), ".");
	}

	//----------------------------------------------------------------------------
	public static String stemOf(File f)
	{
		return Ut.longestPrefixOf(f.getName(), ".");
	}

	//----------------------------------------------------------------------------
	public static File fExtended(String path, String ext)
	{
		return new File(
				path + (Ut.suffixOf(path, ".").equalsIgnoreCase(ext) ? "" : ("." + ext))
		);
	}

	//----------------------------------------------------------------------------
	public static String longestPrefixOf(String s, String after)
	{
		try { return s.substring(0, s.lastIndexOf(after)); }
		catch (Exception e) { return s; }
	}

	//----------------------------------------------------------------------------
	public static String suffixOf(String s, String before)
	{
		try { return s.substring(s.lastIndexOf(before) + before.length()); }
		catch (Exception e) { return ""; }
	}

	//----------------------------------------------------------------------------
	public static String join(List stringables, String glue)
	{
		if (stringables == null || stringables.size() == 0)
			return "";

		StringBuffer sb = new StringBuffer(stringables.get(0).toString());
		for (int i = 1, n = stringables.size(); i < n; ++i)
		{
			sb.append(glue);
			sb.append(stringables.get(i));
		}

		return sb.toString();
	}

	//----------------------------------------------------------------------------
	/** A mapping function that adds 'delta' to each member of an array. */
	public static int[] mapAdd(int[] a, int delta)
	{
		int[] b = new int[a.length];
		for (int i = 0, n = b.length; i < n; ++i)
			b[i] = a[i] + delta;
		return b;
	}

	//----------------------------------------------------------------------------
	public static int[] identityArray(int n)
	{
		int[] a = new int[n];
		for (int i = 0; i < n; ++i)
			a[i] = i;
		return a;
	}

	//----------------------------------------------------------------------------
	public static int indexOfMin(int[] vals)
	{
		int imin = 0;
		for (int i = 0, min = Integer.MAX_VALUE, n = vals.length; i < n; ++i)
			if (vals[i] < min)
			{
				imin = i;
				min = vals[imin];
			}
		return imin;
	}

	//----------------------------------------------------------------------------
	public static Object newModule(String sdefpkg, String classname)
	throws GEException
	{  return newModule(sdefpkg, classname, null); }

	//----------------------------------------------------------------------------
	public static Object newModule( String sdefpkg,
			String classname,
			String suffix )
	throws GEException
	{
		String name = classname;
		if ( suffix != null )
			name += suffix;
		try {
			return Class.forName(
					classname.indexOf('.') == -1 ? (sdefpkg + "." + name ) : name
			).newInstance();
		} catch (Exception e) {
			throw new GEException("Can't get class instance: ", e);
		}
	}

	//----------------------------------------------------------------------------
	public static String strAppend( String s1, String s2, String sep )
	{
		StringBuffer strBuffer = new StringBuffer();
		strBuffer.append(s1);
		strBuffer.append(sep);
		strBuffer.append(s2);
		return strBuffer.toString();
	}

	//----------------------------------------------------------------------------
	public static String array2str( String[] lst, String sep )
	{
		StringBuffer sb = new StringBuffer();
		sb.append(lst[0]);
		for ( int i=1; i < lst.length; i++ )
		{
			sb.append(sep);
			sb.append(lst[i]);
		}
		return sb.toString();
	}

	//----------------------------------------------------------------------------
	public static String array2str( int[] lst, String sep )
	{
		StringBuffer sb = new StringBuffer();
		sb.append(Integer.toString(lst[0]));
		for ( int i=1; i < lst.length; i++ )
		{
			sb.append(sep);
			sb.append(Integer.toString(lst[i]));
		}
		return sb.toString();
	}
}
