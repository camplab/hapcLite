package edu.utah.med.genepi.genie;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.NumberFormat;
import java.util.Calendar;

import edu.utah.med.genepi.analysis.Analysis;
import edu.utah.med.genepi.analysis.AnalysisReport;
import edu.utah.med.genepi.hapconstructor.analysis.AnalysisTable;
import edu.utah.med.genepi.stat.QuantitativeTable;
import edu.utah.med.genepi.stat.ResultImp;
import edu.utah.med.genepi.util.Ut;

public class GenieReporter {
	
	private static final int TABLE_WIDTH  = 75;
	private static final int LABEL_FWIDTH = 15;
	private static final int COL_FWIDTH   = 15;
	private static final int DIV_FWIDTH   = 3;
	
	private static int numSim;
	private static Calendar inDate;
	private PrintWriter pwReport, pwSummary, pwSimulated;
	private String[] studyname, peddatasrc;
	private static final NumberFormat numberformat = NumberFormat.getInstance();
	private String pathStem = null;
	private GenieDataSet gs = null;

	//---------------------------------------------------------------------------
	private void summaryReport( Analysis[] a ) throws IOException
	{
		File fsummary = Ut.fExtended(pathStem,"summary");
		pwSummary = new PrintWriter(new BufferedWriter(new FileWriter(fsummary)),true);
		for ( int i = 0; i < a.length; i++ ) summaryCCAnalysis(a[i]);
		closeReport(pwSummary,fsummary);
	}
	
	//---------------------------------------------------------------------------
//	private void simulatedReport( Analysis[] a ) throws IOException
//	{
//		File fsimulated = Ut.fExtended(pathStem,"simulated");
//		pwSimulated = new PrintWriter(new BufferedWriter(new FileWriter(fsimulated)), true);
//		for ( int i = 0; i < a.length; i++ ) simulatedCCAnalysis(a[i]);
//		closeReport(pwSimulated,fsimulated);
//	}
	
	//----------------------------------------------------------------------------
	public void summaryCCAnalysis ( Analysis a )
	{
		int[] loci = a.getLoci();
		String[] markerNames = new String[loci.length];
		for ( int i = 0; i < loci.length; i++ ) 
		{
			String markerName = gs.getLocus(-1,loci[i]).getName();
			if ( markerName.length() == 0 ) markerName = "Marker " + loci[i];
			markerNames[i] = markerName;
		}
		pwSummary.print(Ut.array2str(markerNames," ") + "  " + a.getModel() + "  " + gs.getRSeed() + "  ");

		AnalysisTable[] aTables = a.getTables();
		for ( int i=0; i < aTables.length; i++ )
		{
			pwSummary.print(" " + studyname[i] + " ");
			if ( aTables.length > 1 ) putSummaryStatReports( a.getReport() );
		}
		if ( aTables.length > 1 )
		{
			putSummaryStatReports( a.getReport() );
		}
		pwSummary.println(" ");
	}

	//----------------------------------------------------------------------------
//	public void simulatedCCAnalysis ( Analysis a )
//	{
//		CCAnalysis.Table[][][][] tables = a.getObservedTable();
//		CCAnalysis.Table[][][] metaTDTtables = a.getObsMetaTDTTable();
//		int nMetaReports = a.getMetaReports(0,0).length;
//		int nMetaTDTReports = a.getMetaTDTReports(0,0).length;
//
//		int nrepeat = a.getRepeat();
//		String markers = new String();
//		for ( int r = 0; r < nrepeat; r++ ) {
//			int[] loci_id = a.getLoci(r);
//			for ( int i = 0; i < loci_id.length; i++ ) {
//				String markerName = gdef.getLocus(loci_id[i]).getMarker();
//				if ( markerName.length() == 0 )
//					markerName = "Marker " + loci_id[i];
//				markers += markerName + " ";
//			}
//			pwSimulated.print(markers + "  " + a.getModel() + "  " + seed + "  ");
//
//			for ( int t = 0; t < tables[r].length; t++ ) {
//				for ( int s = 0; s < tables[r][t].length; s++ )
//					putSimulatedStatReports( a.getStatReports(r, t, s) );
//
//				if ( nMetaReports > 0 )
//					putSummaryStatReports( a.getMetaReports(r, t) );
//				if ( nMetaTDTReports > 0 )
//					putSummaryStatReports( a.getMetaTDTReports(r, t) );
//				pwSummary.println(" ");
//			}
//		}
//	}
	
	
	//---------------------------------------------------------------------------
	void reportHeaderInfo()
	{
		System.out.println("\nWriting Report.....");
		pwReport.println("********** " + gs.getAppId() + gs.getAppVersion() + " Report **********");
		pwReport.println("Created: " + gs.getInCal().getTime());
		pwReport.println("Specification from: " + gs.getRgenFileName());
		pwReport.println("Data from: " + peddatasrc[0]);
		int nStudy = gs.getNStudy();
		for ( int i = 1; i < nStudy; i++ ) pwReport.println("           " + gs.getDataSourceName(i));
		pwReport.println();
	}
	
	//----------------------------------------------------------------------------
	private void putFullStatReports(AnalysisReport[] reports)
	{
		for (int i = 0; i < reports.length; ++i)
		{
			int[] validSim = reports[i].getNumSimulationReport(); 
			pwReport.print(reports[i].getTitle() + " : "); 
			if ( validSim.length > 0  && !gs.getDistribution().equals("weightedindex") )
			{
				for ( int j = 0; j < validSim.length; j++ ) 
				{
					pwReport.print( validSim[j] + " / " + numSim );
					if ( validSim.length > 1 && j != (validSim.length - 1) )
						pwReport.print(", ");
				}
				pwReport.println(" total statistics calculated");
			}
			else 
				pwReport.println();
			pwReport.println("   Observed statistic : " + 
					setMaxDigits(reports[i].getObservationalReport()));
			if ( reports[i].getObsExtraReport() != null )
				pwReport.println("   " + setMaxDigits(reports[i].getObsExtraReport()));
			pwReport.println("   " + reports[i].getCompStatTitle() + 
					setMaxDigits(reports[i].getInferentialReport()));
			if ( reports[i].getInfExtraReport() != null )
				pwReport.println("   " + reports[i].getInfExtraStatTitle() +
						" : " + setMaxDigits(reports[i].getInfExtraReport()));
			if ( reports[i].getWarning() != null )
				pwReport.println("   " + reports[i].getWarning());
		}
	}

	//----------------------------------------------------------------------------
	private void putSummaryStatReports(AnalysisReport[] reports)
	{
		for (int i = 0; i < reports.length; ++i)
		{
			pwSummary.print(" " + setMaxDigits(reports[i].getObservationalReport()));
			pwSummary.print(" " + setMaxDigits(reports[i].getInferentialReport()));
			if ( reports[i].getInfExtraReport() != null )
				pwSummary.print(" " + setMaxDigits(reports[i].getInfExtraReport()));
		}
	}

	//----------------------------------------------------------------------------
//	private void putSimulatedStatReports(CCStatRun.Report[] reports)
//	{
//		for (int i = 0; i < reports.length; ++i)
//		{
//			pwSimulated.print(" obs " +
//					setMaxDigits(reports[i].getObservationalReport()));
//			//pwSimulated.print(" inf " + reports[i].getInferentialReport());
//			//if ( reports[i].getInfExtraReport() != null )
//			//  pwSummary.print(" " + reports[i].getInfExtraReport());
//			pwSimulated.println();
//
//			if ( reports[i].getInfSimReport() != null )
//			{
//				Vector<Vector<Double>> v = reports[i].getInfSimReport();
//				int nv = v.size();
//				int ncv = v.get(0).size();
//				for (int k = 0; k < ncv; k++ )
//				{
//					pwSimulated.println();
//					//pwSimulated.print("sim : " + k + " " );
//					for ( int j = 0; j < nv; j++ )
//					{
//						double value = v.get(j).get(k);
//						pwSimulated.print(" " + value);
//					}
//				}
//			}
//			pwSimulated.println();
//		}
//	}
	
	//----------------------------------------------------------------------------
	void putElapseTime()
	{
		long[] times = getElapsedTime();
		pwReport.println();
		pwReport.println("Elapse Time : " + times[0] + " h : " + 
				times[1] + " m : " +
				times[2] + " s" );
	}
	//----------------------------------------------------------------------------
	void closeReport(PrintWriter pw, File file)
	{
		pw.close();
		System.out.println("Report written to '" + file + "'." + Ut.N);
	}
	
	//----------------------------------------------------------------------------
	public long[] getElapsedTime()
	{
		long outSecs  = Calendar.getInstance().getTimeInMillis();
		long inSecs   = gs.getInCal().getTimeInMillis();
		long diffSecs = outSecs - inSecs; 
		long hours    = diffSecs / ( 1000 * 60 * 60 );
		long mins     = diffSecs / ( 1000 * 60 ) - hours * 60;
		long secs     = diffSecs / 1000 - mins * 60; 

		return new long[] { hours, mins, secs };
	}
	
	//----------------------------------------------------------------------------
	public String setMaxDigits(String str)
	{
		return str;
	}

	//----------------------------------------------------------------------------
	public String setMaxDigits(double val)
	{  
		if ( Double.isNaN(val) || Double.isInfinite(val) )
			return "-";
		else
			return numberformat.format(val);
	}
}
