//******************************************************************************
// AlleleFreqTopSim.java
//******************************************************************************
package edu.utah.med.genepi.sim;

import java.util.Map;

import edu.utah.med.genepi.gm.AllelePair;
import edu.utah.med.genepi.gm.FreqDataSet;
import edu.utah.med.genepi.gm.GDef;
import edu.utah.med.genepi.gm.Gtype;
import edu.utah.med.genepi.gm.GtypeBuilder;
import edu.utah.med.genepi.hc.compressGtype;
import edu.utah.med.genepi.ped.Indiv;
import edu.utah.med.genepi.ped.PedData;
import edu.utah.med.genepi.ped.PedQuery;
import edu.utah.med.genepi.ped.Study;
import edu.utah.med.genepi.util.EmpiricalRandomizer;
import edu.utah.med.genepi.util.GEException;
import edu.utah.med.genepi.util.Randy;

//==============================================================================
public class AlleleFreqTopSim implements GSimulator.Top {

	protected String samplemethod;
	protected PedQuery.Predicate pInSample = PedQuery.IS_ANY;
	// default to all
	protected GDef gdef;
	protected Indiv[][] founderInds, sampleInds;
	protected int[] nFounders, nSampleInds;
	protected GtypeBuilder gtBuilder;
	protected EmpiricalRandomizer[][] randAlleleAt;
	// protected static Gtype[] pedGtype;
	protected boolean[] hasFrequency;
	protected Randy r;
	protected Study[] study;
	protected int nStudy, nLoci;
	protected int nSim = 0;

  //----------------------------------------------------------------------------
  public void preProcessor () throws GEException
  {}


  //----------------------------------------------------------------------------
  public void setPedData()
  {
    founderInds = new Indiv[nStudy][];
    sampleInds  = new Indiv[nStudy][];
    nFounders   = new int[nStudy];
    nSampleInds = new int[nStudy];

    for ( int i = 0; i < nStudy; i++ )
    {
      PedData pd = study[i].getPedData();
      founderInds[i] = pd.getIndividuals(PedQuery.IS_FOUNDER);
      sampleInds[i]  = pd.getIndividuals(pInSample);
      nFounders[i] = founderInds[i].length;
      nSampleInds[i] = sampleInds[i].length;
    }
      //founderInds = pd.getIndividuals(PedQuery.IS_FOUNDER);
      //nFounders = founderInds.length;
      //sampleInds = pd.getIndividuals(pInSample);
      //nSampleInds = sampleInds.length;
  }

  

  //----------------------------------------------------------------------------
  //public void setNumSimulation(int num)
  //{
  //  nSim = num;
  //}

  // overloaded for hapBuilder
  public void simulateFounderGenotypes( int index, 
                                        compressGtype cGtype,
                                        int step) throws GEException
  {
    System.out.println("WARNING: simulated founders without using compressGtype");
  }

  //----------------------------------------------------------------------------
  public EmpiricalRandomizer[] getAlleleMap(int studyID)
  {
    return randAlleleAt[studyID];
  }
  //----------------------------------------------------------------------------
  //public static Gtype[] getPedGtype()
  //{  return pedGtype; }
}

