//******************************************************************************
// HapMCDropSim.java
//******************************************************************************
package edu.utah.med.genepi.sim;

import java.io.IOException;
import java.util.Map;
import java.util.List;
import java.util.Iterator;
import edu.utah.med.genepi.ped.Study;
import edu.utah.med.genepi.ped.PedData;
import edu.utah.med.genepi.ped.PedQuery;
import edu.utah.med.genepi.ped.Pedigree;
import edu.utah.med.genepi.ped.Marriage;
import edu.utah.med.genepi.ped.Indiv;
import edu.utah.med.genepi.ped.Indiv.GtSelector;
import edu.utah.med.genepi.gm.GDef;
import edu.utah.med.genepi.gm.Gtype;
import edu.utah.med.genepi.gm.GtypeBuilder;
import edu.utah.med.genepi.gm.AllelePair;
import edu.utah.med.genepi.gchapext.GeneticDataSourceImp;
import edu.utah.med.genepi.hc.compressGtype;
import edu.utah.med.genepi.util.GEException;
import alun.genio.GeneticDataSource;
import java.util.Random;

//==============================================================================
public class HapMCDropSim implements GSimulator.Drop 
{
  protected Study[]      study;
  protected GDef         gdef;
  protected Indiv.GtSelector gtOBS, gtSIM;
  protected Pedigree[][] thePeds;
  protected Indiv[][]    descendInds, anyInds;
  public int           nStudy, nLoci;
  public int           nSim;
  protected int[]        nDescendants, nPeds;
  protected GtypeBuilder gtBuilder;
  public byte          missingData;

  //----------------------------------------------------------------------------
  public void preProcessor () throws GEException
  {}


  //----------------------------------------------------------------------------
  public void setPedData()
  {
    descendInds  = new Indiv[nStudy][];
    anyInds      = new Indiv[nStudy][];
    thePeds      = new Pedigree[nStudy][];
    nPeds        = new int[nStudy];
    nDescendants = new int[nStudy];

    for ( int i = 0; i < nStudy; i++ )
    {
      PedData pd = study[i].getPedData();
      thePeds[i] = pd.getPedigrees();
      nPeds[i] = thePeds[i].length;
      descendInds[i] = pd.getIndividuals(PedQuery.IS_DESCENDANT);
      anyInds[i] = pd.getIndividuals(PedQuery.IS_ANY);
      nDescendants[i] = descendInds[i].length;
    }
  }

  //----------------------------------------------------------------------------
  public void setGDef(GDef gd)
  {
    nLoci = gd.getLocusCount();
    gtBuilder = gd.getGtypeBuilder();
    gdef = gd;
    missingData = gd.getAlleleFormat().getMissingData();
  }
   
  //----------------------------------------------------------------------------
  public void setDataSource( PedQuery.Predicate[] querySample, int index )
  {
    HapMCTopSim top = new HapMCTopSim();
    if ( nLoci > 1 )
    {
      for ( int i = 0 ; i < study.length; i++ )
      {
        Indiv.GtSelector selector = Indiv.GtSelector.SIM;
        PedData peddata = study[i].getPedData();
        for ( int j = 0 ; j < querySample.length; j++ )
        {
          Indiv[] sampleIndiv = peddata.getIndividuals( querySample[j], true);
          GeneticDataSource sampleGds = new GeneticDataSourceImp( study[i],
                                                                  sampleIndiv,
                                                                  selector,
                                                                  index,
                                                                  gdef );

          top.setPhasedData(sampleGds, sampleIndiv, selector, index, gdef);
        }
      }
    }
  }
  
  //----------------------------------------------------------------------------
  public void setDataSource( PedQuery.Predicate[] querySample,
                             int index, compressGtype[] cGtypes )
  {
    HapMCTopSim top = new HapMCTopSim();
    if ( nLoci > 1 )
    {
      for ( int i = 0 ; i < study.length; i++ )
      {
        Indiv.GtSelector selector = Indiv.GtSelector.SIM;
        PedData peddata = study[i].getPedData();

        int[] loci = new int[gdef.getLocusCount()];
        for(int j =0; j < gdef.getLocusCount(); j ++ )
        {
          loci[j] = j;
        }

        for ( int j = 0 ; j < querySample.length; j++ )
        {
          Indiv[] sampleIndiv = peddata.getIndividuals( querySample[j], true);
          GeneticDataSource sampleGds = new GeneticDataSourceImp( study[i],
                                                                  sampleIndiv,
                                                                  selector,
                                                                  index,
                                                                  gdef );
          if ( querySample[j] == PedQuery.IS_ANY )
          {
            Indiv[] caseIndiv = peddata.getIndividuals( PedQuery.IS_CASE, true, loci);
            Indiv[] controlIndiv = peddata.getIndividuals( PedQuery.IS_CONTROL, true, loci);
            top.setPhasedData(sampleGds, caseIndiv, selector, index,
                        gdef, cGtypes[i], PedQuery.IS_CASE );
            top.setPhasedData(sampleGds, controlIndiv, selector,
                        index, gdef, cGtypes[i], PedQuery.IS_CONTROL);
          }
          else 
          {
            top.setPhasedData(sampleGds, sampleIndiv, selector, index, 
                              gdef, cGtypes[i], querySample[j] );
          }
        }
      }
    }
  }
}
