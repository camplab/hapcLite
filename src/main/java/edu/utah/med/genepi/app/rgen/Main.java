//******************************************************************************
// Main.java
//******************************************************************************
package edu.utah.med.genepi.app.rgen;

//==============================================================================
/** Contains the main() method that either runs the GUI console or the mainmanager
 *  controls the flow of the program.
 */
public class Main {

	public static void main(String[] args)
	{
			MainManager mm = new MainManager();
			mm.executeGenie(args);
	}
}

