//******************************************************************************
// ScalarCompTracker.java
//******************************************************************************
package edu.utah.med.genepi.stat;

import umontreal.iro.lecuyer.probdist.ChiSquareDist;

//==============================================================================
class ScalarCompTracker implements CCStat.ComparisonTracker {

	protected double r0Val;
	protected int[]  comparisonCount;
	protected int    gteCount;
	public  int[]  notval;
	public  CCStat.Result confIntervals = null;
	public  String messages = null;
	private boolean doEmpirical;
	private int degrees;

	//----------------------------------------------------------------------------
	ScalarCompTracker() {
		this(false);
	}

	ScalarCompTracker(boolean b) {
		doEmpirical = b;
	}
	//----------------------------------------------------------------------------
	public void setResult( CCStat.Result r, int simIndex )
	{
		if ( simIndex == 0 )
		{
			assert r.elementCount() == 1;
			r0Val = r.doubleValues()[0];
			comparisonCount = new int[1];
			notval = new int[1];
			comparisonCount[0] = gteCount = notval[0] = 0;
		}
		else
		{
			assert r.elementCount() == 1;
			if ( r instanceof ResultImp.StringResult ) notval[0]++;
			else 
			{
				comparisonCount[0]++;
				if (Math.abs(r.doubleValues()[0]) >= Math.abs(r0Val)) ++gteCount;
			}
		}
	}

	//----------------------------------------------------------------------------
	//Needs a boolean member to say empirical v. distributional p-value
	public CCStat.Result getComparisonsResult()
	{
		if ( Double.isNaN(r0Val) ) return new ResultImp.StringResult("-");
		if (doEmpirical) {
			return new ResultImp.Real(gteCount / (double) comparisonCount[0] );
		}
		else {
			// do montreal
			// a la

			ChiSquareDist csd = new ChiSquareDist(degrees);
			double csValue = csd.barF(r0Val);
			// System.out.println("df : "+ degreeF + " obs " + r0Val + " result
			// : " + csValue);
			return new ResultImp.Real(csValue);
		}
	}

	//----------------------------------------------------------------------------
	public int[] getComparisonCount(){ return comparisonCount; }

	//----------------------------------------------------------------------------
	public int[] getnotval(){ return notval; }

	//----------------------------------------------------------------------------
	public void setMessages(String inMessages){ messages = inMessages; }

	//----------------------------------------------------------------------------
	public String getMessages(){ return messages; }

	public void setDegreeOfFreedom(int df){ 
		degrees = df;
		
	}
}
